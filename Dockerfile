FROM mariadb:10.5

# hadolint ignore=DL3005
RUN set -eux; \
    apt-get update; \
    apt-get upgrade -y; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*;
